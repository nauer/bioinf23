# # Create a minimal class
# class Car:
#     pass
#
# # Create instance of class Car
# # car1 = Car()
# #
# # print(car1)
# #
# #
# # # Add Attributes
# # car1.type_ = "VW W12 Roadster"
# # car1.speed = 310
# #
# # print(car1.type_, car1.speed)
# #
# # car2 = Car()
# # car3 = Car()
# #
# # # Add Attributes
# # car2.type_ = "Renault Ludo"
# # car2.speed = 148
# #
# # car3.type_ = "Microcar"
# # car3.speed = 49
# #
# # print(car1.type_, car1.speed)
# # print(car2.type_, car2.speed)
# # print(car3.type_, car3.speed)
# #
# # print(car3.__dict__)
# #
#
# class Car:
#     def drive(self, here, to, mode='fastest'):
#         string = "Auto fährt von {} nach {} im Mode {}"
#         print(string.format(here, to, mode))
#
#
#     def refuel(car, quantity, full=True):
#         pass
#
# car1 = Car()
#
# # A reference of car1 is automatically commited to self
# car1.drive(here="A", to="B")  # No self here
#
#
# car1.refuel(10)


class VoltageContext:
    def __init__(self, voltage):
        self.voltage = voltage
        print(f"Set voltage to {self.voltage}V")

    def __enter__(self):
        print("Start context")
        return self.voltage

    def __exit__(self, type, value, traceback):
        print(type, value, traceback)
        print("Set voltage back to 0V")


import contextlib

@contextlib.contextmanager
def set_voltage_ctx(voltage):
    print(f"Set voltage to {voltage}V")
    print("Start context")
    yield voltage
    print("Set voltage back to 0V")


with set_voltage_ctx(240) as voltage:
    print(f"Measure at {voltage}V")