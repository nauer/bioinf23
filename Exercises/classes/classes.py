# class A:
#     def __init__(self, val):
#         # Create class attributes
#         self.A = val
#         self.B = "Test"
#
#
# a = A(5)
#
# # Reference attributes
# print(a.A, a.B)
#
# # Adding attributes dynamically
# a.welt = "Hallo Welt"
# a.__dict__["zahl"] = 100
#
# print(a.__dict__)
#

class B:
    __slots__ = ["A", "B", "C"]

    def __init__(self, val):
        # Create class attributes
        self.A = 5000
        self.B = 10
        self.C = None

        # self.C = 5  # Will fail - C not in __slots__

    def calculate(self, var):
        self.C = self.A * self.B


b = B(5)
print("A", b.C)
b.calculate(4)
print("B", b.C)

# Reference attributes
print(b.A, b.B)

# # Adding attributes dynamically will not work
# # b.welt = "Hallo Welt"
#
# from dataclasses import dataclass
#
#
# @dataclass(slots=True)  # Works from Python 3.10 on
# class C:
#     A: int
#     B: str = "Test"
#
#
# c = C(5)
#
# # Reference attributes
# print(b.A, b.B)
#
# from pympler import asizeof
#
# print(asizeof.asizeof(a))
# print(asizeof.asizeof(b))
# print(asizeof.asizeof(c))
