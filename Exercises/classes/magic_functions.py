class Vector:
    def __init__(self, x, y):
        self.X = x
        self.Y = y

    def __str__(self):
        return f"Vector:\n\tX:{self.X}\n\tY:{self.Y}"

    def __radd__(self, other):  # call __add__ - same code
        return self.__add__(other)

    def __add__(self, other):
        if isinstance(other, Vector):
            return Vector(self.X + other.X, self.Y + other.Y)
        elif isinstance(other, (int, float)):
            return Vector(self.X + other, self.Y + other)
        else:
            return NotImplemented

    def __iadd__(self, other):
        if isinstance(other, Vector):
            self.X += other.X
            self.Y += other.Y
            return self
        elif isinstance(other, (int, float)):
            self.X += other
            self.Y += other
            return self
        else:
            return NotImplemented


a = Vector(x=5, y=2)
b = Vector(x=3, y=3)




c = a + b
d = c + 4

#   l   r
e = 4 + c

#   4 +
#    |
# int.__add__(c{Vector})  -> NotImplementedError
#    + c
#     |
# Vector.__radd__(4)


print(a)
print(b)
print(c)
print(d)
print(e)

e += a
print(e)


class MyList(list):
    def __setitem__(self, key, value):
        print(f"Set {value} in index {key}")
        super().__setitem__(key, value)






l = MyList()

print(l)
l.append(5)
print(l)
l[0] = "Test"
print(l[0])

print(isinstance(l, list))
