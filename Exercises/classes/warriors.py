"""
Implementieren sie die verschiedenen Klassen mit Avatar als Basisklasse. Überschreiben Sie für jede Typ die attack()
Methode und überlegen Sie, wie sie für den jeden Typ, die Eigenschaften am Besten in den Angriff einbauen können. Sie
können auch andere Methoden erstellen, die ihnen sinnvoll erscheinen.
Erweitern Sie __str__ Methode damit sie zum Beispiel auch die Eigenschaften zeigen.
Bisher haben die Avatare kein Geschlecht. Fügen sie eines hinzu. Verwenden sie dieses auch zur __hash__ Berechnung.
Schreiben sie eine Funktion (außerhalb der Klasse), die ein Duell zwischen zwei oder auch mehreren Avataren durchführt.
"""

import random


# Base class
class Avatar:
    def __init__(self, name):
        self.name = name
        self.strength = None
        self.charisma = None
        self.condition = None
        self.movement = None
        self.dexterity = None
        self.experience = 0
        self.armor = 3
        self.items = []

    def __hash__(self):
        return hash(self.name)

    def __str__(self):
        return f"{self.name} the avatar"

    def __getitem__(self, item):
        return self.items[item]

    def __setitem__(self, key, value):
        print(f"Drop {self.items[key]}")
        self.items[key] = value

    def is_alive(self):
        return self.condition > 0

    def add_item(self, item):
        if len(self.items) < 10:
            self.items.append(item)
        else:
            print("Your bag is full. You need to drop items before")

    def attack(self, enemy):
        raise NotImplementedError("To be implemented")

    def dodge(self):
        """Returns a dodge value. If the value is higher than the hit value. Avatar could dodge the hit."""
        raise NotImplementedError("To be implemented")

    def add_damage(self, damage):
        """Handles damage from enemy"""
        raise NotImplementedError("To be implemented")


class Orc(Avatar):
    def attack(self, enemy):
        print("Orc attack")


class Thief(Avatar):
    def attack(self, enemy):
        print("Thief attack")


class Magician(Avatar):
    def attack(self, enemy):
        print("Magician attack")
        # e.g. throw a fireball and heal yourself in the fight

    def healing(self, avatar):
        avatar.condition += self.charisma // 2


class Warrior(Avatar):
    def __init__(self, name):
        super().__init__(name)
        self.strength = random.randint(5, 10)
        self.charisma = random.randint(1, 10)
        self.condition = random.randint(6, 10)
        self.movement = random.randint(6, 10)
        self.dexterity = random.randint(5, 10)

    def __str__(self):
        return f"{self.name} the warrior\n\tstrength: {self.strength}" \
               f"\n\tcharisma: {self.charisma}"

    def __bool__(self):
        return self.is_alive()

    def attack(self, enemy):
        # Can he hit the enemy
        if random.randint(0, self.dexterity) > enemy.dodge():
            enemy.add_damage(random.randint(1,3) * self.strength // 2)

    def dodge(self):
        return random.randint(0, self.dexterity)

    def add_damage(self, damage):
        if damage > self.armor:
            self.condition -= damage - self.armor


warrior1 = Warrior("Herbert")
warrior2 = Warrior("Robert")

print(warrior1)
print(warrior1.charisma)
print(warrior1.condition)

for i in range(15):
    warrior1.add_item("Gold sword")


# Kampf auf Leben und Tod
while warrior1.is_alive() and warrior2.is_alive():
    warrior1.attack(warrior2)
    print(warrior1.condition)
    print(warrior2.condition)

warrior1[5] = "Shield"
print(warrior1[5])

t1 = Thief("Franz")
t1.add_item("gold")

print(t1[0])

t1.attack(warrior1)

eval("4 + 6")

print(hash(warrior1))
print(hash(warrior2))

d = {warrior1: 3}

warrior1.items.append("TEst")

d[warrior1]

if warrior1.is_alive():
    print("warrior is alive")
else:
    print("warrior is death")

