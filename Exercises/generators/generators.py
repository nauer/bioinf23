"""Iterators and Generators

An iterator is nothing more than a container object that implements the iterator protocol.

Following 2 methods must be implemented:
  * __next__()
  * __iter__()

"""
import random

# Iterators
# Function iter creates an iterator from any sequence
i = iter("ABC")
print(next(i))
print(next(i))
print(next(i))
# print(next(i))  # raise a StopIteration exception

# StopIteration exception signals the end of the Iterator so the loop knows when to stop
for c in iter(["ABC", "DEFG", "HLT"]):
    print(c)

# A loop automatically call iter on a sequence
for c in ["ABC", "DEFG", "HLT"]:
    print(c)


# # Create your own iterator
# class MyNumbers:
#     def __init__(self):
#         self.a = 5
#
#     def __iter__(self):
#         # Normally __iter__ only returns itself
#         self.a = 2
#         return self
#
#     def __next__(self):
#         if self.a == 10:
#             raise StopIteration
#
#         x = self.a
#         self.a += 1
#         return x
#
#
# # Create a instance of class MyNumbers
# myclass = MyNumbers()
#
# # __init__ and then __next__ is called
# print(next(myclass))
# print(next(myclass))
# print(next(myclass))
#
# # __init__, __iter__ and then __next__ is called
# myiter = iter(MyNumbers())
# print(next(myiter))
# print(next(myiter))
# print(next(myiter))
#
# # myiter is reset in the loop because it calls __iter__
# print("Before loop", myiter.a)
# for a in myiter:
#     print("from loop", a)
# print("After loop", myiter.a)


# # Generators / Coroutines / Lazy Evaluation
# # Generators allows a function to pause and return later.
# def generator():
#     # returns to caller similar to return but remember position
#     yield 10
#     # continue from here in the next call
#     yield 12
#     # lacy evaluation of the expression - abs is called at the time when yield returns
#     yield abs(-1)
#     yield "Last value"
#     # Generator is consumed and raise StopIteration
#
#
# g = generator()
# print(next(g))
# print(next(g))
# print(next(g))
# print(next(g))
# # print(next(g))  # raise StopIteration exception
#
#
# Generators can also have a return statement which close the generator.
# def generator():
#     yield 10
#     yield 12
#     return "Oh no"
#     yield abs(-1)
#     yield "Last value"
#
#
# g = generator()
# print(next(g))
# print(next(g))
# # print(next(g))  # raise StopIteration exception
#
# # Get the return value of the generator
# # Return value is stored in the StopIteration exception
# try:
#     next(g)
# except StopIteration as e:
#     print(e.value)
#
#
# def data_generator(tries=10):
#     for i in range(tries):
#         number = random.randint(0, 36)
#
#         if number == 0:
#             print("Zéro")
#
#         yield number
#
#
# for i in data_generator():
#     print(i)
#
# COUNT = 35
#
#
# # Recursive Fibonacci algorithm
# def fibonacci_recursive(n):
#     if n <= 1:
#         return n
#     else:
#         # Recursive call
#         fib = fibonacci_recursive(n - 1) + fibonacci_recursive(n - 2)
#         return fib
#
#
# print("Fibonacci sequence recursive:")
# for i in range(COUNT):
#     print(i, fibonacci_recursive(i))  # Try with 1000 to get recursive depth issue
#
#
# # Fibonacci generator
# def generator_fibonacci():
#     yield 0
#     a, b = 0, 1
#     while True:
#         yield b
#         a, b = b, a + b
#
#
# print("Fibonacci sequence generator:")
# fib = generator_fibonacci()
#
# for i in range(COUNT):
#     print(i, next(fib))
#
#
# # compare efficiency of algorithm
# def get_fibonacci(count):
#     g = generator_fibonacci()
#     for i in range(count + 1):
#         r = next(g)
#     return r
#
# # Run in iPython shell
# # %timeit fibonacci_recursive(30)
# # %timeit get_fibonacci(30)

# # Take a look it into the itertools module
# import itertools
#
# gen = itertools.cycle([1,2,3,"---"])
#
# for i in range(15):
#     print(next(gen))

# gen1, gen2 = itertools.tee("ABCDEFG", 2)
#
# print(next(gen1))
# print(next(gen1))
# print(next(gen2))
# print(next(gen2))

# Geneartor as contextmanager
#
# from contextlib import contextmanager
#
# @contextmanager
# def my_ctx():
#     # __enter__
#     print("Open Context - run before")
#     yield
#     # __exit__
#     print("Close Context - run after")
#
# with my_ctx():
#     print("Do some context dependent stuff")
