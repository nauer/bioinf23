"""
Create a fasta file parser

Create a generator function which takes a fasta file. The generator should return the only one sequence in time.
"""
import dataclasses

FASTA_FILE_PATH = "fasta.fa"


@dataclasses.dataclass()
class FastaSeq:
    header: str
    sequence: str
    type: str
    organism: str


def extract_organism(header):
    """Extracts organism string from header

    Parameters
    ----------
    header: str
        Fasta sequence header

    Returns
    -------
    str
        Name of Organism
    """
    left_index = header.find("[") + 1
    right_index = header.rfind("]")

    return header[left_index:right_index]


def parse_fasta(file_path, sequence_type="RNA"):
    """Function return a generator processing one sequence in time."""
    sequence = []
    header = ""
    sequence_count = 0
    with open(file_path) as f_in:
        for line in f_in:
            line = line.strip()

            # Is line header
            if line[0] == ">":
                if header:
                    sequence_count += 1
                    yield {"Header": header, "Sequence": "".join(sequence)} # FastaSeq(header, "".join(sequence), sequence_type, extract_organism(header))
                    sequence.clear()
                header = line
                continue

            sequence.append(line)

        # Return last sequence
        yield {"Header": header, "Sequence": "".join(sequence)} # FastaSeq(header, "".join(sequence), sequence_type, extract_organism(header))


if __name__ == "__main__":
    fasta_generator = parse_fasta(FASTA_FILE_PATH, "Protein")

    for seq in fasta_generator:
        print(seq["Header"])
        print(seq["Sequence"])
        #print(seq.sequence[:20], "..." if len(seq.sequence) > 20 else "", sep="")
        #print(f"Sequence has a length of {len(seq.sequence)}")
