import numpy as np
import imageio


PATH_IMAGE = "rocket_start.png"
PATH_IMAGE_MSG = "rocket_start_save_msg_in_lowest_bit.png"


def load_image_as_array(file_path):
    try:
        im = imageio.imread(file_path)
    except ValueError:
        print(f"Image {file_path} does not exist")
    return np.array(im)


def get_pixel_colors(image):
    for row in image:
        for pixel in row:
            for index, color in enumerate(pixel):
                yield index, color, pixel


def get_8_pixel_data(image):
    pixel_gen = get_pixel_colors(image)

    while True:
        a_byte = []
        for i in range(8):
            try:
                a_byte.append(next(pixel_gen))
            except StopIteration:
                return

        yield a_byte


def inject_character(pixel_byte, char_ord: int):
    for bit_index in range(8):
        color_index, color_value, pixel = pixel_byte[bit_index]

        if char_ord & 1 << bit_index:
            # Set lowest bit to 1 if not yet set
            pixel[color_index] = color_value | 1  # 128
        else:
            # Set lowest bit to 0 Maximal 8 bit 2**8 -1 (0b11111110)
            pixel[color_index] = color_value & 254  # 127


def extract_character(pixel_byte):
    byte = ""
    for bit_index in range(8):
        color_index, color_value, pixel = pixel_byte[bit_index]

        if color_value & 1:  # 128:
            byte += "1"
        else:
            byte += "0"
    return int(byte[::-1], 2)


def inject_message_in_image(image, message: str):
    # Add null byte to signal end of message
    message += "\x00"
    b_message = message.encode()

    if (len_msg := len(b_message)) > get_max_msg_length(image):
        raise ValueError(
            f"Message is {len_msg - get_max_msg_length(image)} bytes too long to save in picture"
        )

    pixel_color_gen = get_8_pixel_data(image)
    for letter in b_message:
        try:
            inject_character(next(pixel_color_gen), letter)
        except StopIteration:
            print("Message too long for image")

    return image


def extract_message_from_image(image):
    pixel_color_gen = get_8_pixel_data(image)

    msg = []
    for index, pixel_data in enumerate(pixel_color_gen):
        character = extract_character(pixel_data)

        # Check if end of message \x00
        if character == 0:
            break
        msg.append(character)

    return bytes(msg).decode()


def save_image_to_path(image, file_path):
    imageio.imsave(file_path, image)


def get_max_msg_length(image):
    # All color channels divided by 8 - 1 character to signal end of message
    # This is only true for bytes strings - eg. ä needs to 2 bytes to store
    return image.size // 8 - 1


if __name__ == "__main__":
    msg = "A" * 192167
    # Load image
    image = load_image_as_array(PATH_IMAGE)
    # Inject message into image
    image = inject_message_in_image(image, msg)
    # Save new image to file
    save_image_to_path(image, PATH_IMAGE_MSG)
    # Load image again
    image = load_image_as_array(PATH_IMAGE_MSG)
    # Decrypt message
    message = extract_message_from_image(image)
    print(message), len(message)
    print(get_max_msg_length(image))

    print(image.shape)