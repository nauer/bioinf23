import io
import sys

# f_out = open("data.txt", "rb")
# f_out.write("Test")
# f_out.close()
#
# with open("data.txt", "w") as f_out:
#     f_out.write("Test")
#
# with open("data.txt") as f_in:
#     print(f_in.read())


# stream = io.StringIO()
#
# count = stream.write("Hello World")
# #print(stream.getvalue())
#
# print("Good bye", file=stream)
#
# #print(stream.getvalue())
#
# stream.seek(5, 2)
# print(stream.read())
#
no_stream = ""

print(id(no_stream))
no_stream += "Hello"
# Got different id because new string was created
print(id(no_stream))