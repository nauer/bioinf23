# For random numbers
import random


WORD_LIST = "/home/nauer/Projects/FH21-22/master_21-22/bioinf23/Exercises/words.txt"


def get_word():
    with open(WORD_LIST) as f_in:
        words = f_in.readlines()

    random_index = random.randint(0, len(words))

    return words[random_index][:-1]


def get_input():
    while True:
        # Retrieve user input
        character = input("Geben Sie einen Buchstaben ein:\n\t").lower()
        if len(character) == 1:
            break
        else:
            print("Bitte nur einen Buchstaben eingeben!\n")

    return character


def check_for_letter(word, character, hidden_word):
    word_lower = word.lower()
    # Check if letter in word
    index = word_lower.find(character)
    # TODO check if letter is already used
    # Is character in word (-1 character not found)
    if index > -1:
        found_character_positions = []
        # find out at which indices in word
        while index != -1:
            found_character_positions.append(index)
            # index + 1 to find the next character position
            index = word_lower.find(character, index + 1)

        for i in found_character_positions:
            prefix = hidden_word[:i]
            suffix = hidden_word[i + 1:]
            hidden_word = prefix + word[i] + suffix
    else:
        return None

    return hidden_word


def ask_for_repeat():
    # Test request is correct
    while True:
        # Retrieve user input
        request = input("Wollen sie noch ein weiteres Spiel wagen? (j/n) ").lower()

        if request == "j":
            return True
        elif request == "n":
            return False


def draw_hangman():
    pass


if __name__ == "__main__":
    while True:
        word = get_word()

        word_length = len(word)
        hidden_word = "_" * word_length

        max_retries = 10
        retries = 0
        while True:
            character = get_input()

            result = check_for_letter(word, character, hidden_word)
            if result is None:
                print("Diesen Buchstaben gibt es nicht im gesuchten Wort")

                draw_hangman()
                retries += 1  # same as `retries = retries + 1`
                print(f"Sie haben noch {max_retries - retries} Versuche übrig!\n")
            else:
                hidden_word = result

            print(hidden_word)

            # Check if max_retries is reached
            if retries >= max_retries:
                print("Sie haben verloren")
                break

            if word == hidden_word:
                print("Sie haben gewonnen")
                break

        # Exit game
        if not ask_for_repeat():
            print("Bis zum nächsten mal")
            break





