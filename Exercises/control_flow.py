"""Control flow with loops and tuple unpacking"""

# For loop through sequence
for item in ["Test1234", "ABC"]:
    print(item)

# For loop through sequence of numbers 0..9
for i in range(10):
    print(i)

# No new scope is introduced by for loop
i = "Test"

# i (Test) is overwritten by loop
for i in range(5):
    # Inside loop
    print(i)
    print(i + i)

# Outside of loop
print(i)

# Same with while loop
i = 8

while i < 10:
    print(i)
    i += 1

# break and continue
for i in range(10):
    if i % 2 == 0:  # Modulo division
        continue    # Jump into the next iteration

    if i == 6:
        break       # Leave loop and jumps to next
                    # statement after the loop
    print(i)


i = 0

while i < 10:
    print(i)
    i += 1
    break
else:
    print("All done")


# Without enumerate an extra variable index is needed
my_list = [10, 20, 1, 5, "ende", 1.5, 100, 0]

index = 0

for item in my_list:
    print(index, item)
    index += 1

    if index > 3:
        break

# With enumerate no extra variable is needed
for enum in enumerate(my_list):  # (0, 10) (1, 20) (2, 1)
    print(enum)

    if enum[0] > 3:
        break


# Without enumerate with alternative start index
index = 3

for item in my_list:
    print(index, item)
    index += 1

    if index > 3:
        break

# With enumerate with alternative start index
for index, item in enumerate(my_list, start=3):
    print(index, item)

    if index > 3:
        break


# Tuple unpacking
a, b, c = ["a", 4, 6]

print(a)
print(b)
print(c)

# Switching values from x to y
x = 1
y = 2

# Without tuple unpacking a temporary variable is needed
temp = x
x = y
y = temp

print(x, y)

# With tuple unpacking only one line is needed
(x, y) = (y, x)

print(x, y)

# Braces are not needed
x, y = y, x


# Tuple unpacking with variable length of *c
l1 = [1, 2, 3, 4, 5, 6]
l2 = [1, 2, 3, 4, 5, 6, 7, 8, 9]

a, _, b, *c, d = l2

print(a)
print(_)
print(b)
print(c)
print(d)
