# integer = 10
# decimal = 20
# id_ = "test"
#
# print(type(integer))
# print(type(3/1))
# print(id(5))
#
# l = [2,4,"34",[0,1,2,3,4]]
# print(l[3][0])
#
# v1 = [["cat","Katze"], ["animal", "Tier"]]
#
# print(v1[1])
# print(v1[-2])
#
# l = list(range(0,9))
# print(l)
# print(l[7:3:-2])
#
# print(l.count(4))
#
# print((10000).bit_length())
# s = range(3,5)
# print(type(s), s)
#
# l.append("A")
# l.extend(["B", "C"])
# l.append(["B", "C"])
# l.append(l)
# print(l)
#
# import numpy as np
#
# a = np.array([1,2,3,4])
#
# l.append(a)
#
# print(l)
#
# print(l[-1])
#
# a = l[-1]
# print(type(a))
#
# l = list()
# l = []
# t = tuple()
# t = (4,5)
#
# dup = [6,1,2,3,4,5,6,3,4,5,5]
# dup_free = list(set(dup))  # Order will be changed
#
# s1 = {1,2,3,2,3}
# s2 = {2,3,4}
#
# print(s1.symmetric_difference(s2))
#
# print(dup, dup_free)
# print(hash("S"))

pop = [1,3,4]
bip = [100,300,40]
countries = ["austria", "germany", "usa"]
print(pop[2], countries[2])

print(list(zip(countries,pop,pop)))
d_countries = dict(zip(countries, pop))
print(d_countries)

d2 = {"austria":{"bip":100, "pop":1,"language":["wienerisch", "tirolerisch"]}, "germany":{"bip":300, "pop":3}}
print(d2["austria"]["language"][1])

v2d = {"x":3,"y":2}
v3d = {"x":3,"y":2, "z":-3}

print(v2d, v3d)
print(type(v2d), type(v3d))

from dataclasses import dataclass
from typing import List

@dataclass(frozen=True)
class Vector2D:
    x: int
    y: int
    abc: List

l = [1,2]
v1 = Vector2D(x=5,y=-4, abc=l)
l.append(4)

print(v1, type(v1))

from collections import namedtuple

Vector2D = namedtuple("Vector2D", ["x", "y"])


# surename
# forename
# hobbies
# address
# email

user1 = {"surename":"Bauer", "forename":"Pia","email":"pia.bauer@gmx.at","address":{"plz":1212,"town":"Wunderhausen"}, "hobbies":["reading", "writeing"]}
user2 = {"surename":"Bauer", "forename":"Max","email":"max.bauer@gmx.at","address":{"plz":1212,"town":"Wunderhausen"}, "hobbies":["reading", "sleeping", "playing"]}
address_book = {1001: user1, 1003: user2}

print(type(address_book))

for user_id in address_book:
    print(user_id)
    print(address_book[user_id]["forename"])
    print(address_book[user_id]["hobbies"])


print(len(address_book[1003]["hobbies"]))

Student = namedtuple("Student", ["surename","forename","email","address","hobbies"])
Address = namedtuple("Address", ["plz","town"])

user1 = Student("Mustermann","Max","max@gmx.at",Address(2212,"Großengersdorf"),["reading"])

print(user1)


