"""Wrapper class for abyss-pe"""
import subprocess
from pathlib import Path
import argparse

TOOL_PATH = "/usr/bin/abyss-pe"

# abyss-pe k=48 -C abyss name=k48 in="read1_val_1.fq.gz read2_val_2.fq.gz"


class AbyssPe:
    """abyss-pe Python wrapper"""
    def __init__(self, abyss_pe_tool_path):
        self.tool = abyss_pe_tool_path
        self.results = []

    def run(self, job_name_prefix, k_mer_sizes, working_directory, input_files):
        procs = []

        for k_mer_size in k_mer_sizes:
            procs.append(subprocess.Popen([
                self.tool,
                f"k={k_mer_size}",
                "-C",
                working_directory,
                f"name={job_name_prefix}_{k_mer_size}",
                f"in={' '.join(input_files)}",
            ], stdout=subprocess.PIPE))

        for proc in procs:
            self.results.append(proc.communicate())  # return value = stdout, stderr

    def plot_results(self):
        self.results

def parse_abyss_arguments():
    parser = argparse.ArgumentParser(description="Abyss Description")
    parser.add_argument("-f", "--from", dest="from_", type=int, help="Start value", default=20)
    parser.add_argument("-t", "--to", type=int, help="End value", default=50)
    parser.add_argument("-s", "--step", type=int, help="Step between Start and sTop", default=5)
    parser.add_argument("-W", "--working-directory", help="Working directory", required=True)
    parser.add_argument("reads", nargs="+", help="reads")
    args = parser.parse_args()
    print(args)
    return args


def main():
    arguments = parse_abyss_arguments()

    tool = AbyssPe(TOOL_PATH)
    workdir = Path(arguments.working_directory)
    workdir = workdir.expanduser().absolute()
    print(workdir)
    tool.run("Job", range(arguments.from_, arguments.to, arguments.step), workdir, arguments.reads)
    print(len(tool.results))
    stdout, sdterr = tool.results[3]
    tool.plot_results()



if __name__ == '__main__':
    main()