#!/usr/bin/env python3

import argparse

parser = argparse.ArgumentParser(description="Process some integers.")
# Positional argument
parser.add_argument(
    "integers", metavar="N", type=int, nargs="+", help="an integer for the accumulator"
)
# Optional argument
parser.add_argument(
    "--sum",
    dest="accumulate",
    nargs="*",
    help="sum the integers (default: find the max)",
)

args = parser.parse_args(["--sum", "e","r", "--", "1", "3", "4", "5"])
#args = parser.parse_args(["--help"])
print(vars(args))

print(args.integers)
print(args.accumulate)

