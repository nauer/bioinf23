from contextlib import contextmanager


# # Using hash
# class Person:
#     def __init__(self, name, date_of_birth, id):
#         self.name = name
#         self.birthday = date_of_birth
#         self.id = id
#
#     # If hash is defined, define __eg__ too
#     def __hash__(self):
#         return hash((self.name, self.birthday, self.id))
#
#     # def __eq__(self, other):
#     #     return hash(self) == hash(other)
#
#
# a = Person("Franz", "25.09.1980", 10)
# b = Person("Franz", "25.09.1980", 10)
# c = Person("Franz", "25.09.1981", 13)
# d = Person("Franz", "25.09.1981", 13)
# e = Person("Franz", "25.09.1981", 13)
# f = Person("Franz", "25.09.1981", 13)
#
# s = {"A": a,"B":b,"C":c}
#
# s["F"] = d
# s["Z"] = e
# s["G"] = f
#
# print(hash(s["A"]))
# s["A"].name = "Franziska"
# print(hash(s["A"]))

# # a.name = "AB"
# # s["D"] = b
#
# print("Equal:")
# print(a == b)
# print(a == c)
# print("Unequal:")
# print(a != b)
# print(a != c)
#
#
# print("Hashes:")
# # str, bytes are salted (Hashes change between Python sessions)
# print(hash(a), hash(b), hash(c), sep="\n")
# # float's are not salted (Hashes do not change between Python sessions)
# print(hash(1.5))
#
# print("Ids:")
# print(id(a), id(b), id(c), sep="\n")
#
# d = a
# print(id(a), id(d), sep="\n")
#
#
# # Open Questions:
# GLOBAL_VARIABLE = "TEST"
# GAP_VALUE = 5
#
#
# s = range(10)
# print(s)
#
# print(list(s))
#
# for i in range(10):
#     print(i)
#     if i == 5:
#         break
#
#
# def func():
#     return 10, 20
#
# a,b = func()
#
# print(a)
# print(b)
#
#
#
# print("Hello" if a == 10 else "Bye")

# def div(a, b):
#     try:
#         a / b
#     except Exception:
#         print("Division failed")
#
# div(5, 0)
# div("5", 2)
#
# def div(a, b):
#     try:
#         a / b
#     except ZeroDivisionError as e:
#         print(f"Division failed: {e}")
#     except TypeError as e:
#         print(f"Arguments have to be from type numeric: {e}")
#     except Exception:
#         print("Unknown Error")
#
#
# div(5, 0)
# div("5", 2)
#
# def div(a, b):
#     return a / b
#
# def higher_level_function(a, b):
#     try:
#         return div(a, b) + div(b, a)
#     except ZeroDivisionError as e:
#         print("Division failed: ", e)
#     except TypeError as e:
#         print("Arguments have to be from type numeric: ", e)
#     except Exception as e:
#         print("Unknown Error: ", e)
#
# higher_level_function(5, 0)
#
# def div(a, b):
#     try:
#         print("Open an expensive database resource")
#         a / b
#         print("Close database resource")
#     except Exception as e:
#         print("Unknown Error: ", e)
#
# # Database is not closed if an error occur
# div(5, 0)

# def div(a, b):
#     try:
#         print("Open an expensive database resource")
#         a / b
#     except Exception as e:
#         print("Unknown Error: ", e)
#     finally:
#         print("Close database resource")
#
# # Database is closed even if an error occur
# div(5, 0)


# @contextmanager
# def my_ctx():
#     try:
#         print("Create expansive database connection")
#         yield
#     finally:
#         print("Close Connection")
#
#
# with my_ctx():
#     5 / 0


# def div(a, b):
#     try:
#         print("Open an expensive database resource")
#         a / b
#     except Exception as e:
#         print("Unknown Error: ", e)
#     else:
#         print("All done without errors!")
#     finally:
#         print("Close database resource")
#
# # Database is closed even if an error occur
# div(5, 0)


# def validate_input(a):
#     if not isinstance(a, str):
#         raise TypeError("A must be from type str")
#
#
# try:
#     validate_input(10)
# except TypeError as e:
#     print(f"ERROR: {e}")
#

# class AccountException(Exception):
#     """My Exception"""
#
#
# raise AccountException("Account is empty")

class Account:
    def __init__(self, user, balance):
        self.user = user
        self.balance = balance


class AccountException(ValueError):
    def __init__(self, user, balance, message):
        self.user = user
        self.balance = balance
        self.msg = message


def withdraw(account):
    if account.balance < 0:
        raise AccountException(account.user, account.balance, "Balance is negative")


account = Account("Herbert", -1000)

try:
    withdraw(account)
except AccountException as e:
    print(f"{e.msg} for account {e.user}: {e.balance}€")

