# Write a load csv function for the olympia data set. Consider a structure (dict/list, dataclass) to store the data.
import csv
import collections
import pathlib
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.colors as mcolors

BASE_PATH = pathlib.Path("/home/nauer/Projects/FH21-22/master_21-22/bioinf23/Exercises/olympics/")
ATHLETES_PATH = pathlib.Path("Athletes.csv")
ENTRIES_GENDER_PATH = pathlib.Path("EntriesGender.csv")
MEDALS_PATH = pathlib.Path("Medals.csv")

Athletes = collections.namedtuple('Athletes', 'Name Country Discipline')


def load_athletes(file_path):
    """Load olympic athletes data set.

    Parameters
    ----------
    file_path : str
        Path to dataset

    Returns
    -------
    List[Dicts]
        Athletes data set
    """
    with open(file_path) as csvfile:
        reader = csv.reader(csvfile)
        lines = []
        for index, row in enumerate(reader):
            if index == 0:
                continue
            lines.append({"Discipline": row[0], "Female": row[1], "Male": row[2], "Total": row[3]})

    return lines


def load_medals(file_path):
    with open(file_path) as csvfile:
        reader = csv.reader(csvfile)
        medals = {}
        for index, row in enumerate(reader):
            if index == 0:
                continue

            medals[row[1]] = {"Rank": row[0], "Gold": row[2], "Silver": row[3], "Bronze": row[4]}

    return medals



def load_entries_gender(file_path):
    with open(file_path) as csvfile:
        reader = csv.reader(csvfile)
        lines = []
        for index, row in enumerate(reader):
            if index == 0:
                continue
            lines.append({"Discipline": row[0], "Female": row[1], "Male": row[2], "Total": row[3]})

    return lines


def get_austrian_athlets(athletes):
    austrian_athlets = []
    for athlet in athletes:
        if athlet["Country"] == "Austria":
            austrian_athlets.append(athlet)

    return austrian_athlets

# etc. ...

# Try to answer following questions:
# * Tell me all Austrian athletes
# * Which discipline has the most entries?
# * Which disciplines have equal women and men entries?
# * Are there entries with more women than men?
# * How many gold medals in sum were collected?
# * Who many medals would we have if we sum up all European countries?
# Write a function for each question ans sub functions if needed.
european_countries = [
    "Austria",
    "Italy",
    "Belgium",
    "Latvia",
    "Bulgaria",
    "Lithuania",
    "Croatia",
    "Luxembourg",
    "Cyprus",
    "Malta",
    "Czechia",
    "Netherlands",
    "Denmark",
    "Poland",
    "Estonia",
    "Portugal",
    "Finland",
    "Romania",
    "France",
    "Slovakia",
    "Germany",
    "Slovenia",
    "Greece",
    "Spain",
    "Hungary",
    "Sweden",
    "Ireland",
]

# Make some nice plots e.g. barplot of countries and medals.

# Save all results as csv or other format.


if __name__ == "__main__":
    print(BASE_PATH.exists())
    #athletes = load_athletes(BASE_PATH / ATHLETES_PATH)
    entries = load_entries_gender(BASE_PATH / ENTRIES_GENDER_PATH)
    medals = load_medals(BASE_PATH / MEDALS_PATH)
    #print(len(athletes))
    print(len(entries))
    #print(athletes[9]["Name"])
    print(medals)
    #print(get_austrian_athlets(athletes))

    countries = european_countries[:6]

    gold = []
    silver = []
    bronze = []

    for country in countries:
        gold.append(medals[country]["Gold"])
        silver.append(medals[country]["Silver"])
        bronze.append(medals[country]["Bronze"])



    x = np.arange(len(countries))  # the label locations
    width = 0.1  # the width of the bars

    fig, ax = plt.subplots()
    rects1 = ax.bar(x - width, gold, width, label='Gold', color="gold")
    rects2 = ax.bar(x, silver, width, label='Silver', color="silver")
    rects3 = ax.bar(x + width , bronze, width, label='Bronze', color="y")


    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel('Total')
    ax.set_title('Medals')
    ax.set_xticks(x)
    ax.set_xticklabels(countries)
    ax.legend()

    ax.bar_label(rects1, padding=3)
    ax.bar_label(rects2, padding=3)
    ax.bar_label(rects3, padding=3)
    #
    fig.tight_layout()
    #
    plt.show()
