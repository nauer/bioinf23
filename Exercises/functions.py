# Without functions
# Print a boxed caption
h1 = "Hello World!"
dash_line = "-" * (len(h1) + 4)
print(dash_line)
print(f"| {h1} |")
print(dash_line)

# Python supports unicode \u2665 or \N{Black Heart Suit}
h2 = "I \u2665 Python!"
dash_line = "-" * (len(h2) + 4)
print(dash_line)
print(f"| {h2} |")
print(dash_line)


# Function for structuring code
def print_boxed_caption(word):
    dash_line = "-" * (len(word) + 4)
    print(dash_line)
    print(f"| {word} |")
    print(dash_line)


print_boxed_caption("Functions are good for structure code")
print_boxed_caption("Functions reduce/avoid redundent code")


# No return defined
def print_boxed_caption(word, character="*"):
    strip = character * (len(word) + 4)
    print(f"{strip}\n{character} {word} {character}\n{strip}")


return_value = print_boxed_caption("Hello")

print(type(return_value))
print(return_value is None)


# No printing but returning
def print_boxed_caption(word, character="*"):
    strip = character * (len(word) + 4)
    return f"{strip}\n{character} {word} {character}\n{strip}"


caption = print_boxed_caption("Test")
print(type(caption))
print(caption)


# Call a function by positional parameters
def abc(a, b, c):
    print(a, b, c)


# Positional paramters are identified by their postions
abc("A", "B", "C")  # a="A", b="B", c="C"
abc("A", "C", "B")  # a="A", b="C", c="B"

# You need to set a value for each parameter
# abc("A")            # TypeError


# Call a function by keyword parameters

# If keyword parameters are used they must
# be placed behind positional parameters
abc("A", c="C", b="B")  # a="A", b="B", c="C"

# "A" and "B" can not assigned by position because c="C" is on the first position
# abc(c="C", "A", "B")    # SyntaxError

# Optionale parameters can but don't need to be set
def say_hello(name, greeting="Hello", leave="Bye!"):
    return f"{greeting} {name}! {leave}"


say_hello("Fritz")  # name = "Fritz", greeting="Hello", leave="Bye!"
say_hello("Fritz", "Buongiorno")  # name = "Fritz", greeting="Buongiorno", leave="Bye!"

# Because leave is the 3rd parameter and the 2nd parameter is left empty keyword assignment is needed
say_hello(
    "Fritz", leave="Arrivederci!"
)  # name = "Fritz", greeting="Hello", leave="Arrivederci!"


# print can take any number of positional values
print(1, "A", "Test")

# but has also other parameters too. For that you have to use keyword assignment
print(1, "A", "Test", sep="-")
help(print)


# *pos_rest allows arbitrary positional parameters and collects all positional parameters as tuple
# **key_rest allows arbitrary keyword paramters and collects all keyword parameters as dict

# Paramter group order is important
def abc(pos1, *pos_rest, key1, **key_rest):
    print("Positional:", pos1)
    print("Pos Rest:", pos_rest)
    print("Keyword:", key1)
    print("Key Rest:", key_rest)
    print(type(pos_rest))
    print(type(key_rest))


abc(1, 2, key1="k1", key2="k2")
abc(1, 2, 3, 4, key1="k1", key2="k2", key3="K1", key4="K2")

# Example
config = {
    "voltage_range": (2, 10),
    "width": 10,
    "height": 15,
    "Igun": 23.56,
    "pattern": "circle002.png",
}


def set_window_size(width, height):
    print(f"window_size:\n\twidth={width}\n\theight={height}")


# set_window_size(**config) fails too many paramters


# kwargs fetch all not needed paramters
def set_window_size(width, height, **kwargs):
    print(f"window_size:\n\twidth={width}\n\theight={height}")


set_window_size(**config)


# Tuple and dict unpacking *, **
my_list = ["Hallo", "Werner", "wie geht es dir"]
my_dict = {"sep": "--"}

print(my_list[0], my_list[1], my_list[2], sep=my_dict["sep"])
# Same as above
print(*my_list, **my_dict)


# Immutable parameter
param = "Immutable data type str"
print(param)


def func_i(p1):
    p1 = "Defined in func_i"
    print(p1)


# What is the value of param
func_i(param)
print(param)


# Mutable parameter
para = ["Mutable data type list"]
print(para)


def func_m(p1):
    p1[0] = "Defined in func_m"
    print(p1)


func_m(para)
print(para)


# Never use a mutable object as default parameter
def func_o(key, value, d={}):
    d[key] = value
    print(d)


func_o("A", 100)
# Function does not start with an empty dict
func_o("B", 100)


def count_calls(count=[0]):
    count[0] += 1
    print(f"Function is called {count[0]} times")


count_calls()
count_calls()
count_calls()


# Scopes
x = 1  # Global (Module) scope


def func_scope():
    x = 20   # Function (local) scope
    print(x)


print(x)
func_scope()
# What is the value of x
print(x)

# No scope change
x = 1
print(x)

# Does not create its own scope
# x is still in global scope
for x in range(10):
    y = x

print(x)
print(y)  # Although defined in loop it is in global scope


# Global scope
g_var = "I'm in global scope"


def func_scope():
    print(g_var)  # find g_var in global scope


func_scope()
print(g_var)


# Unbound local error
g_var = "I'm in global scope"


def func_scope():
    print(g_var)  # UnboundLocalError
    g_var = "New assign from func_scope"


# func_scope()  # fails because of UnboundLocalError
print(g_var)


# Shadowing global variable
g_var = "I'm in global scope"


def func_scope():
    # local variable g_var shadows global variable g_var
    g_var = "New assign from func_scope"
    print(g_var)


func_scope()
print(g_var)


# Use global to assign values to global defined variables from function
g_var = "I'm in global scope"


def func_scope():
    # Use global variable g_var instead of local variable
    global g_var  # explicitly set to global
    print(g_var)
    g_var = "New assign from func_scope"


func_scope()
print(g_var)  # Is now changed from function


# Nonlocal scope
g_var = "I'm in global scope"


def funcA():
    n_var = 5
    print(g_var, n_var)

    def funcB():
        global g_var
        nonlocal n_var  # Change n_var from enclosing function
        g_var = "New assign from funcB"
        n_var = 2 * n_var

    funcB()
    print(g_var, n_var)

funcA()
# There is no funcB in global space
# It is also not possible to access a function variable from an outer scope


# LEGB rule
a = "I'm in global scope"
b = "I'm also in global scope"


def func_outer():
    b = "I'm nonlocal"

    def func_inner():
        print("func_inner:", a, " - ", b)

    func_inner()
    print("func_outer:", a, " - ", b)


func_outer()
print("global", a, " - ", b)